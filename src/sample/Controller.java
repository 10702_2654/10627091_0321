package sample;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser;

import java.io.*;
import java.net.URL;
import java.util.ResourceBundle;

public class Controller implements Initializable {
    public DatePicker dpBrithday;
    public Label sex;
    public RadioButton rdM;
    public ToggleGroup sexGroup;
    public RadioButton rdG;
    public ImageView myAvatct;
    public Button btSelectAvatar;
    public GridPane mycontrolller;
    public ComboBox<String> cbBlood;
    public ComboBox<String> rbCounty;

    public void doSelectBrithday(ActionEvent actionEvent) {
    System.out.println(dpBrithday.getValue());
    }

    public void doSelectsex(ActionEvent actionEvent) {
        RadioButton radioButton=(RadioButton) actionEvent.getSource();
        System.out.println(radioButton.getText());
    }

    public void doSelectAvatar(ActionEvent actionEvent) {
        FileChooser fileChooser=new FileChooser();
        fileChooser.setTitle("Open Resource File");
        fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("Image File","*.png","*.jpg","*.gif"));
        File selectedFile = fileChooser.showOpenDialog(mycontrolller.getScene().getWindow());
        System.out.println(selectedFile.getAbsolutePath());

        myAvatct.setImage(new Image(selectedFile.toURI().toString()));
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        ObservableList<String> items = FXCollections.observableArrayList();
        items.addAll("A","O","P");
        cbBlood.getItems().addAll(items);

        ObservableList<String> itemplaces =FXCollections.observableArrayList();

        String filename ="C:\\Users\\CSIE-E518\\Desktop\\County.txt";
        String line ;
        try{
            BufferedReader bufferedReader =new BufferedReader(new InputStreamReader(new FileInputStream(filename)));
            while ((line=bufferedReader.readLine())!=null){
                itemplaces.add(line);
            }
            bufferedReader.close();
            }
        }catch(FileNotFoundException e){

            }

    }

    }




}
